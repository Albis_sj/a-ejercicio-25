import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  //output
  text: string = ''
  mensaje1!: string;
  text2: string = ''
  mensaje2!: string;
  text3 : string = '';
  mensaje3!: string;

  constructor() { }

  ngOnInit(): void {
  }

  recibirMensaje($event: any): void{
    this.mensaje1 = $event;
    console.log(this.mensaje1);
  }

  recibirM2($event: string): void{
    this.mensaje2 = $event;
    console.log(this.mensaje2);
  }
  
  recibirm3($event: string): void{
    this.mensaje3 = $event;
    console.log(this.mensaje3);
  }

  btn1(): void{
    this.text = this.mensaje1
  }
  
  btn2(): void{
    this.text2 = this.mensaje2
  }

  btn3(): void{
    this.text3 = this.mensaje3
  }
}
