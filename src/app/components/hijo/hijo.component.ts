import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  //Output
  mensaje: string = 'Mensaje del hijo al padre';
  @Output() EventoMensaje = new EventEmitter<string>()

  mensaje2: string = 'segundo Mensaje';
  @Output() Evento2 = new EventEmitter<string>()

  mensaje3: string = 'tercer Mensaje';
  @Output() Evento3 = new EventEmitter<string>()

  constructor() { }

  ngOnInit(): void {
    this.EventoMensaje.emit(this.mensaje)
    this.Evento2.emit(this.mensaje2)
    this.Evento3.emit(this.mensaje3)
  }

}
